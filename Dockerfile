FROM python:3.8-alpine

# Actualizo e instalo paquetes varios
RUN apk update 
RUN apk upgrade
RUN apk add curl libxml2-dev rust cargo libxslt-dev python3-dev build-base unixodbc-dev unzip gcc chromium chromium-chromedriver libffi-dev openssl-dev
RUN curl -O https://download.microsoft.com/download/e/4/e/e4e67866-dffd-428c-aac7-8d28ddafb39b/msodbcsql17_17.6.1.1-1_amd64.apk
RUN curl -O https://download.microsoft.com/download/e/4/e/e4e67866-dffd-428c-aac7-8d28ddafb39b/mssql-tools_17.6.1.1-1_amd64.apk
RUN apk add --allow-untrusted msodbcsql17_17.6.1.1-1_amd64.apk --allow-untrusted mssql-tools_17.6.1.1-1_amd64.apk

# Actualizo pip
RUN pip install selenium
RUN pip install --upgrade pip

# Copio los archivos
RUN mkdir /it0491-mercury-automation
COPY requirements.txt /it0491-mercury-automation
WORKDIR /it0491-mercury-automation

# Instalo dependencias con pip
RUN pip install -r requirements.txt

# Setteo un par de configuraciones para OpenSSL
RUN echo "MinProtocol = TLSv1.0" >> /etc/ssl/openssl.cnf
RUN echo "CipherString = DEFAULT@SECLEVEL=1" >> /etc/ssl/openssl.cnf

# Ejecuto el instalador del driver de MSSQL
RUN odbcinst -j

# Agrego variables de entorno
ENV PLATFORM=Docker
ENV VERSION=v1

# Hago un tail a /dev/null para que no muera el container antes de tiempo
CMD tail -f /dev/null
